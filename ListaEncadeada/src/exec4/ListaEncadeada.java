package exec4;

public class ListaEncadeada {

	private Nodo primeiro;
	private Nodo ultimo;
	private Nodo ponteiro;
	private static int posicao=0;
	
	
	public void add(int valor) {
		Nodo novoNodo = new Nodo();
		novoNodo.setValor(valor);
		novoNodo.setPosicao(posicao);
		posicao++;
		
		if (primeiro == null && ultimo == null) {
			primeiro = novoNodo;
			ultimo = novoNodo;
		}else {
			ultimo.setProximo(novoNodo);
			ultimo = novoNodo;
		}
	}
	
	private Nodo anterior;
	
	public int search(int valor) {
		int result=0;
		
		if(primeiro.getValor()==valor) {
			result=primeiro.getPosicao();	
		}else {
			anterior=primeiro;
			primeiro=primeiro.getProximo();
		}
		
		if (result == 0 && hasNext()) {
			search(valor);	
		}
		
		return result;
	}
	
	public String positionValue() {
		String positionV = "Posi��o: "+ponteiro.getPosicao()+ "- valor: "+ponteiro.getValor() ;
		return positionV;
	}
	
	public boolean hasNext() {
		if (primeiro == null) {
			return false;
		}else if(ponteiro == null) {
			ponteiro = primeiro;
			return true;
		}else {
			boolean hasNext = ponteiro.getProximo() != null ? true : false;
			ponteiro = ponteiro.getProximo();
			return hasNext;
		}
		
	}

	public Nodo getPonteiro() {
		return ponteiro;
	}

	public void setPonteiro(Nodo ponteiro) {
		this.ponteiro = ponteiro;
	}
	
	
	
}
