package exec1;

public class ListaEncadeada {

	private Celula primeiro;
	private Celula ultimo;
	private Celula ponteiro;

	public void add(Professor professor) {
		Celula celula = new Celula();
		celula.setValor(professor);

		if (primeiro == null && ultimo == null) {
			primeiro = celula;
			ultimo = celula;
		} else {
			ultimo.setProximo(celula);
			ultimo = celula;
		}
	}

	public void remove(String nome) {
		Celula anterior = null;
		Celula ref = primeiro;

		while (ref != null && !ref.getValor().getNome().equals(nome)) {
			anterior = ref;
			ref = ref.getProximo();
		}

		if (ref == null) {
			IllegalArgumentException erro = new IllegalArgumentException("Dado n�o encontrado");
			throw erro;
		}

		if (anterior == null) {
			primeiro = ref.getProximo();
		} else {
			anterior.setProximo(ref.getProximo());
		}

	}

	public void removeAll() {
		primeiro = null;
	}

	public long size() {
		long contador = 0;
		while (temProximo()) {
			contador++;
		}
		return contador;
	}

	public boolean isEmpty() {
		return (primeiro == null) ? true : false;
	}

	public boolean temProximo() {
		if (primeiro == null) {
			return false;
		} else if (ponteiro == null) {
			ponteiro = primeiro;
			return true;
		} else {
			boolean temProximo = ponteiro.getProximo() != null ? true : false;
			ponteiro = ponteiro.getProximo();
			return temProximo;
		}

	}

	public Celula getPonteiro() {
		return ponteiro;
	}

	public void setPonteiro(Celula ponteiro) {
		this.ponteiro = ponteiro;
	}
	
	

}
