package exec1;

import java.util.Scanner;

public class Program {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		ListaEncadeada listaEncadeada = new ListaEncadeada();
		add(sc,listaEncadeada);
		System.out.println("Tamanho da lista: "+listaEncadeada.size());
		print(listaEncadeada);
		//------------------------
		System.out.println("Informe um nome para excluir: ");
		String nome = sc.nextLine();
		listaEncadeada.remove(nome);
		System.out.println("Tamanho da lista: "+listaEncadeada.size());
		print(listaEncadeada);
		//------------------------
		System.out.println("Lista esta vazia? "+listaEncadeada.isEmpty());
		System.out.println("\nRemovendo todos os professores");
		listaEncadeada.removeAll();
		System.out.println("Lista esta vazia? "+listaEncadeada.isEmpty());
		System.out.println("Tamanho da lista: "+listaEncadeada.size());
		
		sc.close();
	}
	
	public static void add(Scanner sc, ListaEncadeada listaEncadeada) {
		boolean op;

		do {
			System.out.print("Informe o nome do professor: ");
			String nomeProfessor = sc.nextLine();			
			
			listaEncadeada.add(new Professor(nomeProfessor));
			
			System.out.print("Deseja cadastrar um outro professor? s/n: ");
			char resp = sc.nextLine().charAt(0);
			op = (resp == 's') ? true : false;
		} while (op);

	}
	
	public static void print(ListaEncadeada listaEncadeada) {
		System.out.println("A lista tem os seguintes professores: ");
		while (listaEncadeada.temProximo()) {
			System.out.println(listaEncadeada.getPonteiro().getValor().toString());
		}
	}
}
