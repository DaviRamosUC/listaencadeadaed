package exec3;

public class ListaEncadeada {

	private Nodo primeiro;
	private Nodo ultimo;
	private Nodo ponteiro;
	
	
	public void add(int valor) {
		Nodo novoNodo = new Nodo();
		novoNodo.setValor(valor);
		
		if (primeiro == null && ultimo == null) {
			primeiro = novoNodo;
			ultimo = novoNodo;
		}else {
			ultimo.setProximo(novoNodo);
			ultimo = novoNodo;
		}
	}
	
	public String positionValue(int cont) {
		String positionV = "Posi��o: "+cont+ "- valor: "+ponteiro.getValor() ;
		return positionV;
	}
	
	public boolean hasNext() {
		if (primeiro == null) {
			return false;
		}else if(ponteiro == null) {
			ponteiro = primeiro;
			return true;
		}else {
			boolean hasNext = ponteiro.getProximo() != null ? true : false;
			ponteiro = ponteiro.getProximo();
			return hasNext;
		}
		
	}

	public Nodo getPonteiro() {
		return ponteiro;
	}

	public void setPonteiro(Nodo ponteiro) {
		this.ponteiro = ponteiro;
	}
	
	
	
}
