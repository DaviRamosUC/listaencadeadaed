package exec2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ListaEncadeada listaEncadeada = new ListaEncadeada();
		menu(listaEncadeada);
	}

	public static void menu(ListaEncadeada listaEncadeada) {

		int op = 0;
		while (op != 6) {

			System.out.println("Qual opera��o deseja fazer? ");
			System.out.print("\n(1) Adicionar \n(2) Remover \n(3) Pesquisar "

					+ "\n(4) Listar \n(5) Ordenar \n(6) Sair");
			System.out.print("\nEscolha: ");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				add(listaEncadeada);
				break;
			}
			case 2: {
				remove(listaEncadeada);
				break;
			}
			case 3: {
				search(listaEncadeada);
				break;
			}
			case 4: {
				listar(listaEncadeada);
				break;
			}
			case 5: {
				sort(listaEncadeada);
				break;
			}
			case 6: {
				System.out.println("\nAt� mais");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Valor n�o esperado: " + op);
				menu(listaEncadeada);
			}
		}

		sc.close();
	}

	public static void add(ListaEncadeada listaEncadeada) {
		System.out.print("Informe o nome do aluno: ");
		String nome = sc.nextLine();
		System.out.print("Informe a matricula do aluno: ");
		String matricula = sc.nextLine();

		listaEncadeada.add(new Aluno(nome, matricula));
	}

	public static void remove(ListaEncadeada listaEncadeada) {
		System.out.print("Informe o nome do Aluno a ser removido: ");
		String nome = sc.nextLine();
		listaEncadeada.remove(nome);
	}
	
	public static void search(ListaEncadeada listaEncadeada) {
		System.out.print("Informe o nome do Aluno a ser pesquisado: ");
		String nome = sc.nextLine();
		Aluno pesquisado = listaEncadeada.search(nome);
		System.out.println(pesquisado);
	}
	
	public static void listar(ListaEncadeada listaEncadeada) {
		while (listaEncadeada.hasNext()) {
			System.out.println(listaEncadeada.getPonteiro().getValor().toString());
		}
	}
	
	public static void sort(ListaEncadeada listaEncadeada) {
		ArrayList<Aluno> lista = new ArrayList<>();
		
		while(listaEncadeada.hasNext()) {
			lista.add(listaEncadeada.getPonteiro().getValor());
		}
		Collections.sort(lista, Comparator.comparing(Aluno::getNome));

		listaEncadeada.removeAll();
		
		for (Aluno aluno : lista) {
			listaEncadeada.add(aluno);
		}
		
	}
}
