package exec2;

public class ListaEncadeada {

	private Nodo primeiro;
	private Nodo ultimo;
	private Nodo ponteiro;

	public void add(Aluno aluno) {
		Nodo novoNodo = new Nodo();
		novoNodo.setValor(aluno);

		if (primeiro == null && ultimo == null) {
			primeiro = novoNodo;
			ultimo = novoNodo;
		} else {
			ultimo.setProximo(novoNodo);
			ultimo = novoNodo;
		}
	}

	public void removeAll() {
		primeiro = null;
		ultimo = null;
	}

	public void remove(String nome) {
		Nodo anterior = null;
		Nodo ref = primeiro;

		while (ref != null && !ref.getValor().getNome().equals(nome)) {
			anterior = ref;
			ref = ref.getProximo();
		}
		if (ref == null) {
			throw new IllegalStateException("Dado n�o encontrado");
		}

		if (anterior == null) {
			primeiro = ref.getProximo();
		} else {
			anterior.setProximo(ref.getProximo());
		}
	}

	public Aluno search(String nome) {
		Aluno result = null;
		while (hasNext()) {
			if (ponteiro.getValor().getNome().equals(nome)) {
				result = ponteiro.getValor();
			}
		}

		return result;
	}

	public boolean hasNext() {
		if (primeiro == null) {
			return false;
		} else if (ponteiro == null) {
			ponteiro = primeiro;
			return true;
		} else {
			boolean hasNext = ponteiro.getProximo() != null ? true : false;
			ponteiro = ponteiro.getProximo();
			return hasNext;
		}

	}

	public Nodo getPonteiro() {
		return ponteiro;
	}

	public void setPonteiro(Nodo ponteiro) {
		this.ponteiro = ponteiro;
	}

}
