package exec2;

public class Nodo {
	
	private Aluno valor;
	private Nodo proximo;
	

	public Aluno getValor() {
		return valor;
	}

	public void setValor(Aluno valor) {
		this.valor = valor;
	}

	public Nodo getProximo() {
		return proximo;
	}

	public void setProximo(Nodo proximo) {
		this.proximo = proximo;
	}
	

}
